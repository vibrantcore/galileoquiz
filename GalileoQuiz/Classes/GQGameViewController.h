//
//  GQGameViewController.h
//  GalileoQuiz
//
//  Created by Gihad Chbib on 09.03.14.
//  Copyright (c) 2014 ProSieben Games. All rights reserved.
//

#import "VMFGameViewController.h"

@interface GQGameViewController : VMFGameViewController

- (id)initWithGame:(VMFGame *)game;

@end
