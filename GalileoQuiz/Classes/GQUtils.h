//
//  GQUtils.h
//  GalileoQuiz
//
//  Created by Gihad Chbib on 04.03.14.
//  Copyright (c) 2014 ProSieben Games. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum GQFontType
{
    GQFontTypeBoldItalic        = 1,
    GQFontTypeBold              = 2,
    GQFontTypeExtraBoldItalic   = 3,
    GQFontTypeExtraBold         = 4,
    GQFontTypeLightItalic       = 5,
    GQFontTypeLight             = 6,
    GQFontTypeMediumItalic      = 7,
    GQFontTypeMedium            = 8,
    GQFontTypeRegularItalic     = 9,
    GQFontTypeRegular           = 10,
    GQFontTypeSemiBoldItalic    = 11,
    GQFontTypeSemiBold          = 12
}
GQFontType;


@interface GQUtils : NSObject

+(UIFont*)fontForType:(GQFontType)type size:(float)size;

@end
