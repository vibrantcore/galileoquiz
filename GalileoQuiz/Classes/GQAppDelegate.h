//
//  GQAppDelegate.h
//  GalileoQuiz
//
//  Created by Gihad Chbib on 01.03.14.
//  Copyright (c) 2014 ProSieben Games. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GQAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
