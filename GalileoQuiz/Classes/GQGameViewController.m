//
//  GQGameViewController.m
//  GalileoQuiz
//
//  Created by Gihad Chbib on 09.03.14.
//  Copyright (c) 2014 ProSieben Games. All rights reserved.
//

#import "GQGameViewController.h"
#import "VMFQuestion.h"
#import "VMFRank.h"
#import "VMFGameQueue.h"
#import "VMFShared.h"
#import "VCUtils.h"
#import "VCLoader.h"
#import "VMFGlobals.h"
#import "VMFEngine.h"
#import "VCMsgBox.h"
#import "Localizable.h"

#define ANIM_MULTIPLIER 0.5f
//#ifdef DEBUG
//#undef ANIM_MULTIPLIER
//#define ANIM_MULTIPLIER 0.1f
//#endif

// Animation design constants
#define kAbortAnimDuration 0.2f
#define kAbortConfirmWaitTime 3.0f

#define kTimeLeftFadeDuration 0.5f*ANIM_MULTIPLIER
#define kProfileMoveDuration 0.5f*ANIM_MULTIPLIER
#define kProfileAnimDelay 0.3f*ANIM_MULTIPLIER
#define kAnswerButtonAnimOffsetX 50

#define kRoundGetReadyAnimDuration 0.5f*ANIM_MULTIPLIER
#define kRoundGetReadyAnimDelay 2.0f*ANIM_MULTIPLIER
#define kQuestionAnimDuration 0.5f*ANIM_MULTIPLIER
#define kAnswerAnimDuration 1.0f*ANIM_MULTIPLIER
#define kQuestionShowDurationBeforeStart 2.0f*ANIM_MULTIPLIER

#define kHideButtonsDuration 0.5f*ANIM_MULTIPLIER
#define kHideFinalButtonDelay 1.0f*ANIM_MULTIPLIER
#define kHideRestDuration 0.5f*ANIM_MULTIPLIER
#define kHideButtonsSingleDelay 0.1f*ANIM_MULTIPLIER

#define kRoundGetReadyLine1Font [UIFont fontWithName:kDefaultFontName size:26]
#define kRoundGetReadyLine2Font [UIFont fontWithName:kDefaultFontName size:18]

#define kScoreBarAnimDuration 1.0f*ANIM_MULTIPLIER
#define kScoreBarAnimDelay 0.5f*ANIM_MULTIPLIER

#define kAnswerSelectionIndicatorFadeDuration 0.2f*ANIM_MULTIPLIER
#define kAnswerSelectionIndicatorPlayer2Delay 0.5f*ANIM_MULTIPLIER

#define kTimeLeftBeforeSound 3
#define kQuestionTimerInterval 1.f

@interface GQGameViewController ()
{
    CGPoint answer1Center;
    CGPoint answer2Center;
    CGPoint answer3Center;
    CGPoint answer4Center;
    
    BOOL appeared;
}

@property (weak, nonatomic) IBOutlet UIView *mainContentView;

@property (weak, nonatomic) IBOutlet UIView *profile1View;
@property (weak, nonatomic) IBOutlet PFImageView *profile1PicImageView;
@property (weak, nonatomic) IBOutlet UILabel *profile1NameLabel;
@property (weak, nonatomic) IBOutlet UILabel *profile1TitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *profile1PointLabel;

@property (weak, nonatomic) IBOutlet UIView *profile2View;
@property (weak, nonatomic) IBOutlet PFImageView *profile2PicImageView;
@property (weak, nonatomic) IBOutlet UILabel *profile2NameLabel;
@property (weak, nonatomic) IBOutlet UILabel *profile2TitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *profile2PointLabel;

@property (weak, nonatomic) IBOutlet UIView *timeLeftView;
@property (weak, nonatomic) IBOutlet UILabel *timeLeftLabel;

@property (weak, nonatomic) IBOutlet UIView *profile1BarView;
@property (weak, nonatomic) IBOutlet UILabel *profile1ScoreLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profile1ScoreBarImageView;

@property (weak, nonatomic) IBOutlet UIView *profile2BarView;
@property (weak, nonatomic) IBOutlet UILabel *profile2ScoreLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profile2ScoreBarImageView;

@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UIButton *answer1Button;
@property (weak, nonatomic) IBOutlet UIButton *answer2Button;
@property (weak, nonatomic) IBOutlet UIButton *answer3Button;
@property (weak, nonatomic) IBOutlet UIButton *answer4Button;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;

@property (weak, nonatomic) IBOutlet UILabel *roundGetReadyLabel;
@property (weak, nonatomic) IBOutlet UIImageView *player1AnswerSelection;
@property (weak, nonatomic) IBOutlet UIImageView *player2AnswerSelection;

@property (weak, nonatomic) IBOutlet UIButton *abortButton;

@property (strong, nonatomic) VMFQuestion *currentQuestion;
@property int questionIndex;
@property int player1Score;
@property int player2Score;
@property NSTimeInterval timeLeft;
@property NSTimer *questionTimer;

@end

@implementation GQGameViewController

#pragma mark - Init methods

- (id)initWithGame:(VMFGame *)game
{
    self = [super initWithGame:game nibName:@"GQGameViewController" bundle:[NSBundle mainBundle]];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appeared = NO;
    // Lock screen until animations are done
    self.mainContentView.userInteractionEnabled = NO;
    
    // Initial values
    self.profile1View.alpha = 0;
    self.profile1NameLabel.text = @"";
    self.profile1ScoreLabel.text = @"140";
    self.profile1PointLabel.text = @"0";
    self.profile1TitleLabel.text = @"";
    self.profile1BarView.alpha = 0;
    
    self.profile2View.alpha = 0;
    self.profile2NameLabel.text = @"";
    self.profile2ScoreLabel.text = @"140";
    self.profile2PointLabel.text = @"0";
    self.profile2TitleLabel.text = @"";
    self.profile2BarView.alpha = 0;
    
    self.roundGetReadyLabel.alpha = 0;
    
    self.timeLeftView.alpha = 0;
    self.timeLeftLabel.text = @"";
    
    self.questionLabel.alpha = 0;
    self.questionLabel.text = @"";
    
    self.answer1Button.alpha = 0;
    self.answer2Button.alpha = 0;
    self.answer3Button.alpha = 0;
    self.answer4Button.alpha = 0;
    self.answer1Button.titleLabel.font = [GQUtils fontForType:GQFontTypeMedium size:15];
    self.answer2Button.titleLabel.font = [GQUtils fontForType:GQFontTypeMedium size:15];
    self.answer3Button.titleLabel.font = [GQUtils fontForType:GQFontTypeMedium size:15];
    self.answer4Button.titleLabel.font = [GQUtils fontForType:GQFontTypeMedium size:15];
    [self.answer1Button setTitle:@"" forState:UIControlStateNormal];
    [self.answer2Button setTitle:@"" forState:UIControlStateNormal];
    [self.answer3Button setTitle:@"" forState:UIControlStateNormal];
    [self.answer4Button setTitle:@"" forState:UIControlStateNormal];
    
    self.player1AnswerSelection.alpha = 0;
    self.player2AnswerSelection.alpha = 0;
    
    // Start at -1 to increment to first question with index 0 at start
    _questionIndex = -1;
    
    // Both players start with a score of 0
    _player1Score = 0;
    _player2Score = 0;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(cancelAndGotoHome)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
}

- (void)viewDidLayoutSubviews
{
    // Remove bottom logo if small 3.5-inch screen is active
    if (!IS_WIDESCREEN) {
        float offsetY = fabsf(self.answer4Button.center.y - self.logoImageView.center.y);
        self.logoImageView.alpha = 0;
        
        // Exception for non-widescreen phones:
        // 1. Hide logo at bottom
        // 2. Shift all answer buttons down to start at position of logo
        // 3. Shift timer up by 15 points
        // 4. Shift question up by 30 points
        NSArray *buttons = @[self.answer1Button, self.answer2Button, self.answer3Button, self.answer4Button];
        [buttons enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            UIButton *btn = obj;
            CGPoint center = btn.center;
            center.y += offsetY;
            btn.center = center;
        }];
        
        CGPoint tc = self.timeLeftView.center;
        tc.y -= 15;
        self.timeLeftView.center = tc;
        
        CGPoint qc = self.questionLabel.center;
        qc.y -= 30;
        self.questionLabel.center = qc;
    }
    
    
    answer1Center = self.answer1Button.center;
    answer2Center = self.answer2Button.center;
    answer3Center = self.answer3Button.center;
    answer4Center = self.answer4Button.center;

    [self updateScore];
}

- (void)viewDidAppear:(BOOL)animated
{
    if (!appeared) {
        appeared = YES;
        if ([self.game.opponent.objectId isEqualToString:[VMFUser currentUser].objectId]) {
            // User is the opponent, so game already exists in backend
            
            [self.abortButton setTitle:@"Aufgeben" forState:UIControlStateNormal];
            if ([self.game.state intValue] != kVMFGameInProgress) {
                // Game is shown for the first time, set state and load first question
                [self saveMatchedGameStartAndContinue];
            } else {
                // Game was shown before, load the question index right after
                // last seen question index in order to prevent cheating
                // and load scores up until now
                int lastQuestionIndex = [self.game.lastSeenQuestionIndex intValue];
                if (lastQuestionIndex < kNumberOfQuestions-1) {
                    NSArray *p1Answers = nil;
                    NSArray *p2Answers = nil;
                    if ([self.game.creator.objectId isEqualToString:[VMFUser currentUser].objectId]) {
                        p1Answers = self.game.creatorAnswers;
                        p2Answers = self.game.opponentAnswers;
                    } else {
                        p1Answers = self.game.opponentAnswers;
                        p2Answers = self.game.creatorAnswers;
                    }
                    for (NSMutableDictionary *answerDict in p1Answers) {
                        _player1Score += [OBJECT_FOR_KEY_WITH_TYPE(answerDict, @"score", NSNumber) intValue];
                    }
                    for (NSMutableDictionary *answerDict in p2Answers) {
                        _player2Score += [OBJECT_FOR_KEY_WITH_TYPE(answerDict, @"score", NSNumber) intValue];
                    }
                    [self loadNextQuestionAfterIndex:lastQuestionIndex];
                    [self showEntryAnimation];
                    
                } else {
                    // If the last seen question was the final one, jump to match result immediately
                    // after setting score to zero for last question
                    [self loadNextQuestionAfterIndex:lastQuestionIndex];
                    [self setAnswerWithScore:0 answer:0 time:0];
                    [self saveAndGotoResults:NO];
                }
            }
        } else {
            // User is the creator, so new game is being created
            [self.abortButton setTitle:GENERAL_CANCEL forState:UIControlStateNormal];
            [self loadNextQuestion];
            [self showEntryAnimation];
        }
    }
}

- (void)updateScore
{
    self.profile1PointLabel.text = [NSString stringWithFormat:@"%d", (int)self.player1Score];
    self.profile2PointLabel.text = [NSString stringWithFormat:@"%d", (int)self.player2Score];
    
    float offsetY = self.profile1ScoreLabel.frame.size.height + 2;
    float mh = self.profile1BarView.frame.size.height - offsetY;
    
    float p1SF      = ((float)self.player1Score)/kMaxScore;
    CGRect f1       = self.profile1ScoreBarImageView.frame;
    f1.size.height  = 1.f + (mh - 1.f)*p1SF;
    f1.origin.y     = mh - f1.size.height + offsetY;
    
    float p2SF      = ((float)self.player2Score)/kMaxScore;
    CGRect f2       = self.profile2ScoreBarImageView.frame;
    f2.size.height  = 1.f + (mh - 1.f)*p2SF;
    f2.origin.y     = mh - f2.size.height + offsetY;
    
    [UIView animateWithDuration:kScoreBarAnimDuration
                          delay:kScoreBarAnimDelay
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.profile1ScoreBarImageView.frame = f1;
                         self.profile2ScoreBarImageView.frame = f2;
                     }
                     completion:nil];
}

- (void)loadNextQuestionAfterIndex:(int)index
{
    _questionIndex = index + 1;
    
    // Set current question index and default answer object
    self.game.lastSeenQuestionIndex = [NSNumber numberWithInt:self.questionIndex];
    [self setAnswerWithScore:0 answer:0 time:0];
    
    _currentQuestion = OBJECT_AT_INDEX(self.game.questionArray, self.questionIndex);
    if (self.currentQuestion) {
        
        self.profile1ScoreLabel.textColor = [UIColor whiteColor];
        self.profile2ScoreLabel.textColor = [UIColor whiteColor];
        
        NSAttributedString *getReadyLine1;
        NSAttributedString *getReadyLine2;
        
        if (self.questionIndex >= (kNumberOfQuestions-1)) {
            getReadyLine1 = [[NSAttributedString alloc]
                             initWithString:@"LETZTE RUNDE!\n"
                             attributes:@{NSFontAttributeName:kRoundGetReadyLine1Font}];
            getReadyLine2 = [[NSAttributedString alloc]
                             initWithString:@"DOPPELTE PUNKTZAHL!"
                             attributes:@{NSFontAttributeName:kRoundGetReadyLine2Font}];
        } else {
            getReadyLine1 = [[NSAttributedString alloc]
                             initWithString:[NSString stringWithFormat:@"%@ %d\n", @"RUNDE", self.questionIndex+1]
                             attributes:@{NSFontAttributeName:kRoundGetReadyLine1Font}];
            getReadyLine2 = [[NSAttributedString alloc]
                             initWithString:@"MACH DICH BEREIT!"
                             attributes:@{NSFontAttributeName:kRoundGetReadyLine2Font}];
        }
        NSMutableAttributedString *getReadyLines = [[NSMutableAttributedString alloc] init];
        [getReadyLines appendAttributedString:getReadyLine1];
        [getReadyLines appendAttributedString:getReadyLine2];
        self.roundGetReadyLabel.attributedText = getReadyLines;
        
        
        // Reset timer for next question
        _timeLeft = kMaxTimePerQuestion;
        self.timeLeftLabel.text = [NSString stringWithFormat:@"%d", (int)self.timeLeft];
        
        // Set question texts
        self.questionLabel.text = self.currentQuestion.question;
        [self.answer1Button setTitle:self.currentQuestion.answer1 forState:UIControlStateNormal];
        [self.answer2Button setTitle:self.currentQuestion.answer2 forState:UIControlStateNormal];
        [self.answer3Button setTitle:self.currentQuestion.answer3 forState:UIControlStateNormal];
        [self.answer4Button setTitle:self.currentQuestion.answer4 forState:UIControlStateNormal];
        self.answer1Button.enabled = YES;
        self.answer2Button.enabled = YES;
        self.answer3Button.enabled = YES;
        self.answer4Button.enabled = YES;
        self.answer1Button.selected = NO;
        self.answer2Button.selected = NO;
        self.answer3Button.selected = NO;
        self.answer4Button.selected = NO;
        
    }
}

- (void)loadNextQuestion
{
    [self loadNextQuestionAfterIndex:self.questionIndex];
}

- (void)setAnswerWithScore:(int)score answer:(int)answer time:(int)time
{
    NSMutableArray *a;
    if ([[VMFUser currentUser].objectId isEqualToString:self.game.creator.objectId]) {
        a = self.game.creatorAnswers;
    } else {
        a = self.game.opponentAnswers;
    }
    
    if (!a) {
        a = [[NSMutableArray alloc] init];
    }
    
    NSMutableDictionary *answerObject  = [@{@"score":[NSNumber numberWithInt:score],
                                            @"time":[NSNumber numberWithInt:time],
                                            @"answer":[NSNumber numberWithInt:answer]} mutableCopy];
    
    // If answer object already exists in array, replace it, otherwise add it
    id obj = OBJECT_AT_INDEX(a, self.questionIndex);
    if (obj) {
        [a replaceObjectAtIndex:self.questionIndex withObject:answerObject];
    } else {
        if ([a count] < self.questionIndex) {
            // Fill up missing answers up until the current question index if necessary
            int diff = self.questionIndex - (int)[a count];
            for (int i=0; i<diff; i++) {
                NSMutableDictionary *emptyAnswerObject  = [@{@"score":@0, @"time":@0, @"answer":@0} mutableCopy];
                [a addObject:emptyAnswerObject];
            }
        }
        [a addObject:answerObject];
    }
    
    if ([[VMFUser currentUser].objectId isEqualToString:self.game.creator.objectId]) {
        self.game.creatorAnswers = a;
    } else {
        self.game.opponentAnswers = a;
    }
}


- (void)showEntryAnimation
{
    // Set profile picture of current player
    self.profile1NameLabel.text     = [VMFUser currentUser].displayName;
    self.profile1TitleLabel.text    = [[VMFRank rank] titleAt:[VMFUser currentUser].titleIndex];
    [VMFShared loadAndSetProfilePicOfUser:[VMFUser currentUser] forImageView:self.profile1PicImageView mask:NO];
    
    
    // Set profile picture of other player
    VMFUser *opponent = nil;
    if ([[VMFUser currentUser].objectId isEqualToString:self.game.opponent.objectId]) {
        opponent = self.game.creator;
    } else if (self.game.opponent) {
        opponent = self.game.opponent;
    }
    self.profile2NameLabel.text     = opponent.displayName;
    self.profile2TitleLabel.text    = [[VMFRank rank] titleAt:opponent.titleIndex];
    [VMFShared loadAndSetProfilePicOfUser:opponent forImageView:self.profile2PicImageView mask:NO];
    
    
    
    // Set initial score bar sizes to maximum for both players for animation effect
    float offsetY = self.profile1ScoreLabel.frame.size.height + 2;
    float mh = self.profile1BarView.frame.size.height - offsetY;
    
    CGRect f1       = self.profile1ScoreBarImageView.frame;
    f1.size.height  = mh;
    f1.origin.y     = mh - f1.size.height + offsetY;
    self.profile1ScoreBarImageView.frame = f1;
    
    CGRect f2       = self.profile1ScoreBarImageView.frame;
    f2.size.height  = mh;
    f2.origin.y     = mh - f2.size.height + offsetY;
    self.profile2ScoreBarImageView.frame = f2;
    
    [self updateScore];
    
    float animDelay = kProfileAnimDelay;
    
    CGPoint profile1Pos = self.profile1View.center;
    self.profile1View.center = CGPointMake(-self.profile1View.frame.size.width, profile1Pos.y);
    [UIView animateWithDuration:kProfileMoveDuration
                          delay:animDelay
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.profile1View.alpha = 1;
                         self.profile1BarView.alpha = 1;
                         self.profile1View.center = profile1Pos;
                     }
                     completion:nil];
    
    
    animDelay += kProfileAnimDelay;
    CGPoint profile2Pos = self.profile2View.center;
    self.profile2View.center = CGPointMake(self.view.frame.size.width+self.profile2View.frame.size.width, profile2Pos.y);
    [UIView animateWithDuration:kProfileMoveDuration
                          delay:animDelay
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         if (self.game.opponent) {
                             self.profile2View.alpha = 1;
                             self.profile2BarView.alpha = 1;
                             self.profile2View.center = profile2Pos;
                         }
                     }
                     completion:^(BOOL finished) {
                         
                         // Animate question after profile pics have been shown
                         [self showNextQuestion];
                         
                     }];
    
}


- (void)showNextQuestion
{
    [UIView
     animateWithDuration:kRoundGetReadyAnimDuration
     delay:0
     options:UIViewAnimationOptionCurveLinear
     animations:^{
         self.roundGetReadyLabel.alpha = 1;
     }
     completion:^(BOOL finished) {
         [UIView
          animateWithDuration:kRoundGetReadyAnimDuration
          delay:kRoundGetReadyAnimDelay
          options:UIViewAnimationOptionCurveLinear
          animations:^{
              self.roundGetReadyLabel.alpha = 0;
          }
          completion:^(BOOL finished) {
              [UIView
               animateWithDuration:kQuestionAnimDuration
               delay:0
               options:UIViewAnimationOptionCurveLinear
               animations:^{
                   self.questionLabel.alpha = 1;
               }
               completion:^(BOOL finished) {
                   [UIView
                    animateWithDuration:kTimeLeftFadeDuration
                    delay:kQuestionShowDurationBeforeStart
                    options:UIViewAnimationOptionCurveLinear
                    animations:^{
                        self.timeLeftView.alpha = 1;
                    }
                    completion:^(BOOL finished) {
                        // Animate answer buttons
                        self.answer1Button.center = CGPointMake(answer1Center.x - kAnswerButtonAnimOffsetX, answer1Center.y);
                        self.answer2Button.center = CGPointMake(answer2Center.x - kAnswerButtonAnimOffsetX, answer2Center.y);
                        self.answer3Button.center = CGPointMake(answer3Center.x - kAnswerButtonAnimOffsetX, answer3Center.y);
                        self.answer4Button.center = CGPointMake(answer4Center.x - kAnswerButtonAnimOffsetX, answer4Center.y);
                        [UIView animateWithDuration:kAnswerAnimDuration
                                              delay:0
                                            options:UIViewAnimationOptionCurveEaseOut
                                         animations:^{
                                             self.answer1Button.alpha = 1;
                                             self.answer1Button.center = answer1Center;
                                         } completion:nil];
                        [UIView animateWithDuration:kAnswerAnimDuration
                                              delay:0.1f
                                            options:UIViewAnimationOptionCurveEaseOut
                                         animations:^{
                                             self.answer2Button.alpha = 1;
                                             self.answer2Button.center = answer2Center;
                                         } completion:nil];
                        [UIView animateWithDuration:kAnswerAnimDuration
                                              delay:0.2f
                                            options:UIViewAnimationOptionCurveEaseOut
                                         animations:^{
                                             self.answer3Button.alpha = 1;
                                             self.answer3Button.center = answer3Center;
                                         } completion:nil];
                        [UIView animateWithDuration:kAnswerAnimDuration
                                              delay:0.3f
                                            options:UIViewAnimationOptionCurveEaseOut
                                         animations:^{
                                             self.answer4Button.alpha = 1;
                                             self.answer4Button.center = answer4Center;
                                         } completion:^(BOOL finished) {
                                             [self enableAnswersAndStartTimer];
                                         }];

                    }];
               }];
          }];
     }];
}

- (void)enableAnswersAndStartTimer
{
    _questionTimer = [NSTimer scheduledTimerWithTimeInterval:kQuestionTimerInterval
                                                      target:self
                                                    selector:@selector(countdown:)
                                                    userInfo:nil
                                                     repeats:YES];
    
    self.mainContentView.userInteractionEnabled = YES;
}


- (void)showSelectedAnswer:(NSInteger)answer
{
    self.mainContentView.userInteractionEnabled = NO;

    UIButton *selectedButton = nil;
    UIButton *correctButton = nil;
    
    // Determine correct button object
    int correctAnswer = [self.currentQuestion.correctAnswer intValue];
    if (correctAnswer == 1) {
        correctButton = self.answer1Button;
    } else if (correctAnswer == 2) {
        correctButton = self.answer2Button;
    } else if (correctAnswer == 3) {
        correctButton = self.answer3Button;
    } else if (correctAnswer == 4) {
        correctButton = self.answer4Button;
    }
    
    // Determine selected button object
    if (correctAnswer == answer) {
        // Correct answer selected
        selectedButton = correctButton;
    } else {
        // Wrong answer selected
        if (answer == 1) {
            selectedButton = self.answer1Button;
        } else if (answer == 2) {
            selectedButton = self.answer2Button;
        } else if (answer == 3) {
            selectedButton = self.answer3Button;
        } else if (answer == 4) {
            selectedButton = self.answer4Button;
        }
        
        // Highlight wrong button
        selectedButton.enabled = NO;
    }
    
    // Set selection indicator for player 1
    if (selectedButton) {
        self.player1AnswerSelection.center = CGPointMake(self.player1AnswerSelection.center.x, selectedButton.center.y);
        [UIView animateWithDuration:kAnswerSelectionIndicatorFadeDuration
                         animations:^{
                             self.player1AnswerSelection.alpha = 1;
                         }];
        
    }
    
    VMFUser *p2 = nil;
    NSArray *p2Answers = nil;
    int p2Score = 0;
    int p2AnswerIndex = 0;
    if ([self.game.creator.objectId isEqualToString:[VMFUser currentUser].objectId]) {
        p2 = self.game.opponent;
        p2Answers = self.game.opponentAnswers;
    } else {
        p2 = self.game.creator;
        p2Answers = self.game.creatorAnswers;
    }
    
    if (p2 && p2Answers) {
        NSDictionary *dictionary = OBJECT_AT_INDEX(p2Answers, self.questionIndex);
        p2Score = [OBJECT_FOR_KEY_WITH_TYPE(dictionary, @"score", NSNumber) intValue];
        p2AnswerIndex = [OBJECT_FOR_KEY_WITH_TYPE(dictionary, @"answer", NSNumber) intValue];
        _player2Score += p2Score;
        
        // Determine selected button object
        selectedButton = nil;
        if (correctAnswer == p2AnswerIndex) {
            // Correct answer selected
            selectedButton = correctButton;
        } else {
            // Wrong answer selected
            if (p2AnswerIndex == 1) {
                selectedButton = self.answer1Button;
            } else if (p2AnswerIndex == 2) {
                selectedButton = self.answer2Button;
            } else if (p2AnswerIndex == 3) {
                selectedButton = self.answer3Button;
            } else if (p2AnswerIndex == 4) {
                selectedButton = self.answer4Button;
            }
            
            // Highlight wrong button
            selectedButton.enabled = NO;
        }
        
        // Set selection indicator for player 1
        if (selectedButton) {
            self.player2AnswerSelection.center = CGPointMake(self.player2AnswerSelection.center.x, selectedButton.center.y);
            [UIView animateWithDuration:kAnswerSelectionIndicatorFadeDuration
                                  delay:kAnswerSelectionIndicatorPlayer2Delay
                                options:0
                             animations:^{
                                 self.player2AnswerSelection.alpha = 1;
                             } completion:nil];
            
        }
    }
    
    
    // Highlight correct button
    correctButton.selected = YES;
    
    // Hide wrong buttons
    float delay = kHideButtonsDuration;
    
    // Hide previously highlighted answer indicators
    [UIView animateWithDuration:kAnswerSelectionIndicatorFadeDuration
                          delay:delay
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.player1AnswerSelection.alpha = 0;
                         self.player2AnswerSelection.alpha = 0;
                     } completion:nil];
    
    if (correctButton != self.answer1Button) {
        [UIView animateWithDuration:kHideButtonsDuration
                              delay:delay
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.answer1Button.alpha = 0;
                             self.answer1Button.center = CGPointMake(answer1Center.x + kAnswerButtonAnimOffsetX, answer1Center.y);
                         } completion:nil];
        delay += kHideButtonsSingleDelay;
    }
    if (correctButton != self.answer2Button) {
        [UIView animateWithDuration:kHideButtonsDuration
                              delay:delay
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.answer2Button.alpha = 0;
                             self.answer2Button.center = CGPointMake(answer2Center.x + kAnswerButtonAnimOffsetX, answer2Center.y);
                         } completion:nil];
        delay += kHideButtonsSingleDelay;
    }
    if (correctButton != self.answer3Button) {
        [UIView animateWithDuration:kHideButtonsDuration
                              delay:delay
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.answer3Button.alpha = 0;
                             self.answer3Button.center = CGPointMake(answer3Center.x + kAnswerButtonAnimOffsetX, answer3Center.y);
                         } completion:nil];
        delay += kHideButtonsSingleDelay;
    }
    if (correctButton != self.answer4Button) {
        [UIView animateWithDuration:kHideButtonsDuration
                              delay:delay
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.answer4Button.alpha = 0;
                             self.answer4Button.center = CGPointMake(answer4Center.x + kAnswerButtonAnimOffsetX, answer4Center.y);
                         } completion:nil];
        delay += kHideButtonsSingleDelay;
    }
    delay += kHideFinalButtonDelay;
    
    // Hide correct button after delay
    [UIView
     animateWithDuration:kHideButtonsDuration
     delay:delay
     options:UIViewAnimationOptionCurveEaseOut
     animations:^{
         correctButton.alpha = 0;
         correctButton.center = CGPointMake(correctButton.center.x + kAnswerButtonAnimOffsetX, correctButton.center.y);
     }
     completion:^(BOOL finished) {

         [UIView
          animateWithDuration:kHideRestDuration
          delay:0
          options:UIViewAnimationOptionCurveEaseOut
          animations:^{
              self.roundGetReadyLabel.alpha = 0;
              self.timeLeftView.alpha = 0;
              self.questionLabel.alpha = 0;
          }
          completion:^(BOOL finished) {
              if (self.questionIndex < (kNumberOfQuestions-1)) {
                  [self loadNextQuestion];
                  if ([self.game.opponent.objectId isEqualToString:[VMFUser currentUser].objectId]) {
                      // If playing as opponent, then this user was matched to another user
                      // To prevent cheating, save the game state immediately after a question
                      // has been shown to the user
                      [self saveGameAndContinue];
                  } else {
                      [self showNextQuestion];
                  }
              } else {
                  if ([self.game.opponent.objectId isEqualToString:[VMFUser currentUser].objectId] &&
                      ([self.game.opponentAnswers count] > 0)) {
                      // Only possible as opponent, because opponent is always the one who
                      // plays the second round (and ends the game), hence goes to results first
                      [self saveAndGotoResults:NO];
                  } else {
                      [self saveAndGotoHome];
                  }
              }
          }];
     
     }];
}


- (void)saveMatchedGameStartAndContinue {
    self.game.state = [NSNumber numberWithInt:kVMFGameInProgress];
    self.game.lastSeenQuestionIndex = @0;
    [self.game saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            // Load first question and show entry animation to start game
            [self loadNextQuestion];
            [self showEntryAnimation];
        } else {
            [VCMsgBox msgBoxWithTitle:GENERAL_ERROR
                                 text:GENERAL_CONNECTION_ERROR
                      leftButtonTitle:GENERAL_CANCEL
              leftButtonTappedHandler:^{
                  
                  // Abort game and goto home without trying to save it again
                  [[NSNotificationCenter defaultCenter]
                   postNotificationName:kVMFNotificationGotoHome
                   object:nil
                   userInfo:@{@"game":self.game}];
                  
              }
                     rightButtonTitle:@"Nochmal"
             rightButtonTappedHandler:^{
                 
                 [self saveMatchedGameStartAndContinue];
             }
                     closeButtonShown:NO
             closeButtonTappedHandler:nil
                            autoClose:YES];
        }
    }];
}

- (void)saveGameAndContinue
{
    self.game.state = [NSNumber numberWithInt:kVMFGameInProgress];
    
    [self.game saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            [self showNextQuestion];
        } else {
            [VCMsgBox msgBoxWithTitle:GENERAL_ERROR
                                 text:GENERAL_CONNECTION_ERROR
                      leftButtonTitle:GENERAL_CANCEL
              leftButtonTappedHandler:^{
                  
                  // Abort game and goto home without trying to save it again
                  [[NSNotificationCenter defaultCenter]
                   postNotificationName:kVMFNotificationGotoHome
                   object:nil
                   userInfo:@{@"game":self.game}];
                  
              }
                     rightButtonTitle:@"Nochmal"
             rightButtonTappedHandler:^{
                 
                 // Try again
                 [self saveGameAndContinue];
             }
                     closeButtonShown:NO
             closeButtonTappedHandler:nil
                            autoClose:YES];
        }
    }];
}


- (void)saveAndGotoResults:(BOOL)aborted
{
    int finishBonus = 0;
    if (aborted) {
        self.questionIndex = kNumberOfQuestions-1;
        [self setAnswerWithScore:0 answer:0 time:0];
        self.game.lastSeenQuestionIndex = [NSNumber numberWithInt:self.questionIndex];
        
        [self.questionTimer invalidate];
        _questionTimer = nil;
        _currentQuestion = nil;
    } else {
        finishBonus = kFinishScoreBonus;
    }
    
    // TODO: Need to check if InApp purchase is active
    int booster = 1;
    
    // User is opponent when going to results
    self.game.opponentFinishBonus = [NSNumber numberWithInt:finishBonus];
    self.game.opponentBoosterMultiplier = [NSNumber numberWithInt:booster];

    self.game.state = [NSNumber numberWithInt:kVMFGameFinishedResultPending];
    
    __block VCLoader *loader = [VCLoader fullscreenLoader];
    [self.game saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        [loader hide];
        if (succeeded) {
            [[NSNotificationCenter defaultCenter]
             postNotificationName:kVMFNotificationGotoResults
             object:nil
             userInfo:@{@"game":self.game}];
        } else {
            [VCMsgBox msgBoxWithTitle:GENERAL_ERROR
                                 text:GENERAL_CONNECTION_ERROR
                      leftButtonTitle:GENERAL_CANCEL
              leftButtonTappedHandler:^{
                  
                  // Abort game and goto home without trying to save it again
                  [[NSNotificationCenter defaultCenter]
                   postNotificationName:kVMFNotificationGotoHome
                   object:nil
                   userInfo:@{@"game":self.game}];
                  
              }
                     rightButtonTitle:@"Nochmal"
             rightButtonTappedHandler:^{
                 
                 // Try again
                 [self saveAndGotoResults:aborted];
             }
                     closeButtonShown:NO
             closeButtonTappedHandler:nil
                            autoClose:YES];
        }
    }];
}

- (void)cancelAndGotoHome
{
    [self.questionTimer invalidate];
    _questionTimer = nil;
    _currentQuestion = nil;
    [[NSNotificationCenter defaultCenter]
     postNotificationName:kVMFNotificationGotoHome
     object:nil
     userInfo:nil];
}

- (void)saveAndGotoHome
{
    NSMutableArray *objectsToSave = [NSMutableArray array];
    if (self.game.opponent) {
        self.game.state = [NSNumber numberWithInt:kVMFGameCreatedWithOpponent];
        [objectsToSave addObject:self.game];
    } else {
        self.game.state = [NSNumber numberWithInt:kVMFGameCreatedWaitingForRandomOpponent];
        VMFGameQueue *gameQueue = [[VMFGameQueue alloc] init];
        gameQueue.game = self.game;
        gameQueue.priority = @0;
        [objectsToSave addObject:self.game];
        [objectsToSave addObject:gameQueue];
    }

    // TODO: Need to check if InApp purchase is active
    int booster = 1;
    
    // User is creator when going to home
    self.game.creatorFinishBonus = [NSNumber numberWithInt:kFinishScoreBonus];
    self.game.creatorBoosterMultiplier = [NSNumber numberWithInt:booster];
    
    [PFObject saveAllInBackground:objectsToSave block:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            [[NSNotificationCenter defaultCenter]
             postNotificationName:kVMFNotificationGotoHome
             object:nil
             userInfo:@{@"game":self.game}];
        } else {
            [VCMsgBox msgBoxWithTitle:GENERAL_ERROR
                                 text:GENERAL_CONNECTION_ERROR
                      leftButtonTitle:GENERAL_CANCEL
              leftButtonTappedHandler:^{
                  
                  // Abort game and goto home without trying to save it again
                  [[NSNotificationCenter defaultCenter]
                   postNotificationName:kVMFNotificationGotoHome
                   object:nil
                   userInfo:@{@"game":self.game}];
                  
              }
                     rightButtonTitle:@"Nochmal"
             rightButtonTappedHandler:^{

                 // Try again
                 [self saveAndGotoHome];
             }
                     closeButtonShown:NO
             closeButtonTappedHandler:nil
                            autoClose:YES];
        }
    }];
}

- (void)countdown:(NSTimer*)timer
{
    if (!self.mainContentView.userInteractionEnabled) {
        // Abort timer update if screen is disabled and animating
        return;
    }
    
    _timeLeft--;
    self.timeLeftLabel.text = [NSString stringWithFormat:@"%d", (int)self.timeLeft];
    if (self.timeLeft <= 0) {
        [self.questionTimer invalidate];
        _questionTimer = nil;
        
        [self showSelectedAnswer:0];
    } else if (self.timeLeft == kTimeLeftBeforeSound) {
        [VMFEngine playSound:kVMFSoundEventTimeRunningOut];
    }
}


#pragma mark - User input

- (IBAction)answerPressed:(id)sender {
    // Stop timer immediately if button is pressed
    [self.questionTimer invalidate];
    _questionTimer = nil;
    
    
    // Highlight the selected answer
    UIButton *btn = sender;
    int selectedAnswer = (int)btn.tag;
    [self showSelectedAnswer:selectedAnswer];
    
    
    // Highlight the score for the answer
    int score = 0;
    int correctAnswer = [self.currentQuestion.correctAnswer intValue];
    if (selectedAnswer == correctAnswer) {
        [VMFEngine playSound:kVMFSoundEventCorrect];
        
        if (self.questionIndex >= (kNumberOfQuestions-1)) {
            score = (int)(kMinScoreFinalQuestion + self.timeLeft*2);
        } else {
            score = (int)(kMinScorePerQuestion + self.timeLeft);
        }
        _player1Score += score;
        self.profile1ScoreLabel.textColor = [btn titleColorForState:UIControlStateSelected];
    } else {
        [VMFEngine playSound:kVMFSoundEventWrong];
        self.profile1ScoreLabel.textColor = [btn titleColorForState:UIControlStateDisabled];
    }
    [self updateScore];
    
    
    // Save score in backend object
    [self setAnswerWithScore:score answer:selectedAnswer time:self.timeLeft];
}

- (IBAction)abortPressed:(id)sender {
    __block UIButton *btn = sender;
    
    if (btn == nil) {
        btn = self.abortButton;
        // Reset button
        btn.tag = 0;
        btn.userInteractionEnabled = NO;
        float posX = btn.frame.origin.x;
        [UIView animateWithDuration:kAbortAnimDuration
                         animations:^{
                             CGRect f = btn.frame;
                             f.origin.x = -f.size.width;
                             btn.frame = f;
                         }
                         completion:^(BOOL finished) {
                             if ([self.game.opponent.objectId isEqualToString:[VMFUser currentUser].objectId]) {
                                 [btn setTitle:@"Aufgeben" forState:UIControlStateNormal];
                             } else {
                                 [btn setTitle:GENERAL_CANCEL forState:UIControlStateNormal];
                             }
                             [UIView animateWithDuration:kAbortAnimDuration
                                              animations:^{
                                                  CGRect f = btn.frame;
                                                  f.origin.x = posX;
                                                  btn.frame = f;
                                              }
                                              completion:^(BOOL finished) {
                                                  btn.userInteractionEnabled = YES;
                                              }];
                             
                         }];
        
    } else {
        
        if (btn.tag == 0) {
            btn.tag = 1;
            btn.userInteractionEnabled = NO;
            float posX = btn.frame.origin.x;
            [UIView
             animateWithDuration:kAbortAnimDuration
             animations:^{
                 CGRect f = btn.frame;
                 f.origin.x = -f.size.width;
                 btn.frame = f;
             }
             completion:^(BOOL finished) {
                 [btn setTitle:@"Sicher?" forState:UIControlStateNormal];
                 [UIView
                  animateWithDuration:kAbortAnimDuration
                  animations:^{
                      CGRect f = btn.frame;
                      f.origin.x = posX;
                      btn.frame = f;
                  }
                  completion:^(BOOL finished) {
                      btn.userInteractionEnabled = YES;
                      // Set an abort timer for the abort button
                      dispatch_after(dispatch_time(DISPATCH_TIME_NOW,
                                                   (int64_t)(kAbortConfirmWaitTime * NSEC_PER_SEC)),
                                     dispatch_get_main_queue(), ^{
                                         [self abortPressed:nil];
                                     });
                  }];
             }];
        } else if (btn.tag == 1) {
            if ([self.game.opponent.objectId isEqualToString:[VMFUser currentUser].objectId]) {
                // Game is being played by user as an opponent
                // which means that abort == give up and goto results
                [self saveAndGotoResults:YES];
            } else {
                // Game is being played by user as creator
                // which means that abort == back to main menu without saving new game
                [self cancelAndGotoHome];
            }
        }
    }
}

#pragma mark - Memory management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationWillResignActiveNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationWillEnterForegroundNotification
                                                  object:nil];
}

@end


