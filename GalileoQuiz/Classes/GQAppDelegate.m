//
//  GQAppDelegate.m
//  GalileoQuiz
//
//  Created by Gihad Chbib on 01.03.14.
//  Copyright (c) 2014 ProSieben Games. All rights reserved.
//

#import "GQAppDelegate.h"
#import "VMF.h"
#import "VMFRank.h"
#import "GQLabel.h"

#import "VMFStartViewController.h"
#import "VMFPanelViewController.h"
#import "VMFMatchUpViewController.h"
#import "VMFMatchResultViewController.h"

#import "GQGameViewController.h"

#import "Localizable.h"

#define kGameViewsAnimDuration 0.5f
#define kGameViewsDelay 0.5f

#define kGotoGameDelay 3.0f

@implementation GQAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    // General design setting for navigation bar
    [[UINavigationBar appearance] setBarStyle:UIBarStyleDefault];
    
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],
                                                           NSFontAttributeName:[UIFont fontWithName:kDefaultFontBoldName size:20]}];
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],
                                                           NSFontAttributeName:[UIFont fontWithName:kDefaultFontName size:17]}
                                                forState:UIControlStateNormal];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"navbar"]
                                       forBarMetrics:UIBarMetricsDefault];
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    [[UITabBar appearance] setBarTintColor:[UIColor whiteColor]];
    [[UITabBarItem appearance] setTitleTextAttributes: @{NSForegroundColorAttributeName:[UIColor whiteColor]}
                                             forState: UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes: @{NSForegroundColorAttributeName:[UIColor whiteColor]}
                                             forState: UIControlStateNormal];
    // Setup VCLoader
    [VCLoader setFullscreenOverlayFadeDuration:0.25f];
    [VCLoader setFullscreenOverlayBackgroundWhite:0];
    [VCLoader setFullscreenOverlayAlpha:0.5];
    
    
    // Init VMFEngine
    [VMFEngine initWithParseAppID:kParseAppID clientKey:kParseClientKey];
    [VMFEngine setGameViewClass:[GQGameViewController class]];
#ifdef DEBUG
    [VMFEngine shared].musicVolume = 0.0f;
    [VMFEngine shared].soundVolume = 1.0f;
#else
    // TODO Music off for prototype version
    [VMFEngine shared].musicVolume = 0.0f;
    [VMFEngine shared].soundVolume = 1.0f;
#endif
    [VMFEngine shared].musicFadeDuration = 0.5f;
    
    
    // Load sounds into engine for usage in game
    [VMFEngine setSound:@"button_click.caf"     forEvent:kVMFSoundEventButtonClick];
    [VMFEngine setSound:@"menu_open.caf"        forEvent:kVMFSoundEventMenuOpen];
    [VMFEngine setSound:@"menu_close.caf"       forEvent:kVMFSoundEventMenuClose];
    [VMFEngine setSound:@"right_answer.caf"     forEvent:kVMFSoundEventCorrect];
    [VMFEngine setSound:@"wrong_answer.caf"     forEvent:kVMFSoundEventWrong];
    [VMFEngine setSound:@"time_ticking.caf"     forEvent:kVMFSoundEventTimeRunningOut];
    [VMFEngine setSound:@"win_jingle.caf"       forEvent:kVMFSoundEventWonGame];
    [VMFEngine setSound:@"gameover_jingle.caf"  forEvent:kVMFSoundEventLostGame];
    
    // Load music into engine for usage in game
    //[VMFEngine setMusic:@"menu.mp3"             forEvent:kVMFMusicTypeMenu];
    //[VMFEngine setMusic:@"game.mp3"             forEvent:kVMFMusicTypeGame];
    
    
    // Register for navigation notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(gotoHome:)
                                                 name:kVMFNotificationGotoHome
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(gotoStart:)
                                                 name:kVMFNotificationGotoStart
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(gotoMatchup:)
                                                 name:kVMFNotificationGotoMatchup
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(gotoGame:)
                                                 name:kVMFNotificationGotoGame
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(gotoResults:)
                                                 name:kVMFNotificationGotoResults
                                               object:nil];
    
    
    // Choose which view controller to show based on User login
    if ([VMFUser currentUser] && VALID_NOTEMPTY([VMFUser currentUser].displayName, NSString)) {
        // If a user is already logged in, go to home screen immediately
        [[VMFUser currentUser] refreshInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        }];
        [self gotoHome:nil];
        
    } else {
        // Otherwise show start screen with login options
        [self gotoStart:nil];
    }
    
    self.window.backgroundColor = [UIColor blackColor];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    // Associate the device with a user
    PFInstallation *installation = [PFInstallation currentInstallation];
    installation[@"user"] = [PFUser currentUser];
    [installation setDeviceTokenFromData:deviceToken];
    [installation saveInBackground];
    NSLog(@"registered device token");

}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"error %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [PFPush handlePush:userInfo];
}

#pragma mark - Custom notifications

-(void)gotoStart:(NSNotification*)notification
{
    VMFStartViewController *startVC = [[VMFStartViewController alloc] init];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:startVC];
    [navController.navigationBar setTintColor:[UIColor whiteColor]];
    [navController.navigationBar setTranslucent:NO];
    
    [VMFEngine playMusic:kVMFMusicTypeMenu looped:YES];
    self.window.rootViewController = navController;
}

-(void)gotoMatchup:(NSNotification*)notification
{
    VMFGame *game = OBJECT_FOR_KEY_WITH_TYPE(notification.userInfo, @"game", VMFGame);
    if (!game) {
        return;
    }
    
    // Create transition from current screen to game match-up screen
    VMFMatchUpViewController *vc = [[VMFMatchUpViewController alloc]
                                    initWithGame:game];

    UIView *view1 = self.window.rootViewController.view;
    UIView *view2 = vc.view;
    
    [UIView animateWithDuration:kGameViewsAnimDuration
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         CGRect f = view1.frame;
                         f.origin.x = -f.size.width;
                         view1.frame = f;
                     }
                     completion:^(BOOL finished) {
                         // Hide status bar for game views
                         [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
                         
                         // Switch to game music
                         [VMFEngine playMusic:kVMFMusicTypeGame looped:YES];
                         
                         self.window.rootViewController = vc;
                         CGRect f = view2.frame;
                         f.origin.y = -view2.frame.size.height;
                         view2.frame = f;
                         [UIView animateWithDuration:kGameViewsAnimDuration
                                               delay:kGameViewsDelay
                                             options:UIViewAnimationOptionCurveEaseOut
                                          animations:^{
                                              CGRect f = view2.frame;
                                              f.origin.y = 0;
                                              view2.frame = f;
                                          }
                                          completion:^(BOOL finished) {
                                              dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kGotoGameDelay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                                  [[NSNotificationCenter defaultCenter]
                                                   postNotificationName:kVMFNotificationGotoGame
                                                   object:nil
                                                   userInfo:@{@"game":game}];
                                              });
                                          }];
                     }];
    
}

-(void)gotoGame:(NSNotification*)notification
{
    VMFGame *game = OBJECT_FOR_KEY_WITH_TYPE(notification.userInfo, @"game", VMFGame);
    if (!game) {
        return;
    }
    
    // Create transition from current screen to game match-up screen
    VMFGameViewController *vc = [[GQGameViewController alloc] initWithGame:game];
    
    UIView *view1 = self.window.rootViewController.view;
    UIView *view2 = vc.view;
    
    [UIView animateWithDuration:kGameViewsAnimDuration
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         CGRect f = view1.frame;
                         f.origin.x = f.size.width;
                         view1.frame = f;
                     }
                     completion:^(BOOL finished) {
                         self.window.rootViewController = vc;
                         view2.alpha = 0;
                         [UIView animateWithDuration:kFadeAnimDuration
                                               delay:kGameViewsDelay
                                             options:0
                                          animations:^{
                                              view2.alpha = 1;
                                          }
                                          completion:nil];
                     }];
}

-(void)gotoResults:(NSNotification*)notification
{
    VMFGame *game = OBJECT_FOR_KEY_WITH_TYPE(notification.userInfo, @"game", VMFGame);
    if (!game) {
        return;
    }
    
    // Create transition from current screen to game match-up screen
    [VMFTopic2User getUserTopicFor:game.topic
                        completion:^(VMFTopic2User *userTopic, NSError *error, BOOL success) {
                            if (success) {
                                VMFMatchResultViewController *resultVC =
                                [[VMFMatchResultViewController alloc] initWithGame:game
                                                                         userTopic:userTopic
                                                                           history:NO];
                                VMFPanelViewController *vc = [[VMFPanelViewController alloc] initWithMidViewController:resultVC];
                                
                                UIView *view1 = self.window.rootViewController.view;
                                UIView *view2 = vc.view;
                                
                                // Show status bar for normal views again
                                [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
                                
                                [UIView animateWithDuration:kGameViewsAnimDuration
                                                      delay:0
                                                    options:UIViewAnimationOptionCurveEaseOut
                                                 animations:^{
                                                     CGRect f = view1.frame;
                                                     f.origin.y = -f.size.height;
                                                     view1.frame = f;
                                                 }
                                                 completion:^(BOOL finished) {
                                                     [VMFEngine playMusic:kVMFMusicTypeMenu looped:YES];
                                                     self.window.rootViewController = vc;
                                                     CGRect f = view2.frame;
                                                     f.origin.x = f.size.width;
                                                     view2.frame = f;
                                                     [UIView animateWithDuration:kFadeAnimDuration
                                                                           delay:kGameViewsDelay
                                                                         options:UIViewAnimationOptionCurveEaseOut
                                                                      animations:^{
                                                                          CGRect f = view2.frame;
                                                                          f.origin.x = 0;
                                                                          view2.frame = f;
                                                                      }
                                                                      completion:nil];
                                                 }];
                            } else {
                                [self gotoHome:nil];
                                [VCMsgBox msgBoxWithTitle:GENERAL_ERROR
                                                     text:@"Konnte das Spiel-Resultat nicht öffnen. Bitte sehen Sie sich das Resultat im Spielverlauf an."
                                        singleButtonTitle:GENERAL_CLOSE];
                            }
                        }];
}

-(void)gotoHome:(NSNotification*)notification
{
    if (![VMFEngine getLoadedCompleteTopics]) {
        
        __block VCLoader *loader;
        __block UIView *container;
        if (!notification) {
            [self goToHomeAfterSyncing:notification];
            container = [[UIView alloc] initWithFrame:self.window.rootViewController.view.bounds];
            container.backgroundColor = [UIColor colorWithWhite:0 alpha:0.7f];
            container.userInteractionEnabled = YES;
            [self.window.rootViewController.view addSubview:container];
            loader = [VCLoader loaderIn:container];
        }else {
            loader = [VCLoader fullscreenLoader];
        }
        GQLabel *infoLabel = [[GQLabel alloc] initWithFrame:CGRectMake(0, 0, 300, 20)];
        [infoLabel setAdjustsFontSizeToFitWidth:YES];
        [infoLabel setTextAlignment:NSTextAlignmentCenter];
        [infoLabel setTextColor:[UIColor whiteColor]];
        [infoLabel setText:GENERAL_LOAD_DATA_FROM_SERVER];
        [infoLabel setCenter:CGPointMake(loader.center.x, loader.center.y-50)];
        if ([loader.container isKindOfClass:[UIView class]]) {
            [(UIView*)loader.container addSubview:infoLabel];
        }
        
        [VMFEngine syncWithCompletion:^(NSError *error, BOOL success) {
            [loader hide];
            if (container) {
                [container removeFromSuperview];
            }
            if (notification) {
                [self goToHomeAfterSyncing:notification];
            }else {
                VMFPanelViewController *cont = (VMFPanelViewController*)self.window.rootViewController;
                if ([((UINavigationController*)cont.centerPanel).topViewController respondsToSelector:@selector(reloadData)]) {
                    [((UINavigationController*)cont.centerPanel).topViewController performSelector:@selector(reloadData)];
                }
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:kVMFNotificationSyncDoneNeedUpdate object:nil userInfo:nil];
        }];
    }else {
        [self goToHomeAfterSyncing:notification];
    }
}

- (void)goToHomeAfterSyncing:(NSNotification*)notification
{
    // Show status bar for normal views again
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    
    VMFGame *game = OBJECT_FOR_KEY_WITH_TYPE(notification.userInfo, @"game", VMFGame);
    if (game) {
        
        // Back to home from game, animate transition
        VMFPanelViewController *vc = [[VMFPanelViewController alloc] init];
        
        UIView *view1 = self.window.rootViewController.view;
        UIView *view2 = vc.view;
        
        [UIView animateWithDuration:kGameViewsAnimDuration
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             CGRect f = view1.frame;
                             f.origin.y = -f.size.height;
                             view1.frame = f;
                         }
                         completion:^(BOOL finished) {
                             
                             // Start menu music
                             [VMFEngine playMusic:kVMFMusicTypeMenu looped:YES];
                             
                             self.window.rootViewController = vc;
                             CGRect f = view2.frame;
                             f.origin.x = -f.size.width;
                             view2.frame = f;
                             [UIView animateWithDuration:kFadeAnimDuration
                                                   delay:kGameViewsDelay
                                                 options:UIViewAnimationOptionCurveEaseOut
                                              animations:^{
                                                  CGRect f = view2.frame;
                                                  f.origin.x = 0;
                                                  view2.frame = f;
                                              }
                                              completion:nil];
                         }];

    } else {
    
        // Make App register for remote notifications
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge|
         UIRemoteNotificationTypeAlert|
         UIRemoteNotificationTypeSound];
        
        [VMFEngine playMusic:kVMFMusicTypeMenu looped:YES];
        self.window.rootViewController = [[VMFPanelViewController alloc] init];
    }
}


#pragma mark - App event handlers

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication withSession:[PFFacebookUtils session]];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBAppCall handleDidBecomeActiveWithSession:[PFFacebookUtils session]];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
