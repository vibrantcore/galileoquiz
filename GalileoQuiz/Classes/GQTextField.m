//
//  GQTextField.m
//  GalileoQuiz
//
//  Created by Gihad Chbib on 31.03.14.
//  Copyright (c) 2014 ProSieben Games. All rights reserved.
//

#import "GQTextField.h"

@implementation GQTextField

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setFont:[GQUtils fontForType:GQFontTypeRegular size:17]];
}

@end
