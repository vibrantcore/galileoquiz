//
//  GQButton.m
//  GalileoQuiz
//
//  Created by Gihad Chbib on 25.03.14.
//  Copyright (c) 2014 ProSieben Games. All rights reserved.
//

#import "GQButton.h"

@implementation GQButton

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    [self.titleLabel setFont:[GQUtils fontForType:(GQFontType)self.tag size:self.titleLabel.font.pointSize]];
}

@end
