//
//  GQTextField.h
//  GalileoQuiz
//
//  Created by Gihad Chbib on 31.03.14.
//  Copyright (c) 2014 ProSieben Games. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GQTextField : UITextField

@end
