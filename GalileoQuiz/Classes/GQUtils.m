//
//  GQUtils.m
//  GalileoQuiz
//
//  Created by Gihad Chbib on 04.03.14.
//  Copyright (c) 2014 ProSieben Games. All rights reserved.
//

#import "GQUtils.h"


@implementation GQUtils

+(UIFont*)fontForType:(GQFontType)type size:(float)size
{
    NSString *fontName = nil;
    switch (type) {
        case GQFontTypeBoldItalic:
            fontName = @"CiutadellaRun-BoldItalic";
            break;
            
        case GQFontTypeBold:
            fontName = @"CiutadellaRun-Bold";
            break;
            
        case GQFontTypeExtraBoldItalic:
            fontName = @"CiutadellaRun-ExtraBoldItalic";
            break;
            
        case GQFontTypeExtraBold:
            fontName = @"CiutadellaRun-ExtraBold";
            break;
            
        case GQFontTypeLightItalic:
            fontName = @"CiutadellaRun-LightItalic";
            break;
            
        case GQFontTypeLight:
            fontName = @"CiutadellaRun-Light";
            break;
            
        case GQFontTypeMediumItalic:
            fontName = @"CiutadellaRun-MediumItalic";
            break;
            
        case GQFontTypeMedium:
            fontName = @"CiutadellaRun-Medium";
            break;
            
        case GQFontTypeRegularItalic:
            fontName = @"CiutadellaRun-RegularItalic";
            break;
            
        case GQFontTypeRegular:
            fontName = @"CiutadellaRun";
            break;
            
        case GQFontTypeSemiBoldItalic:
            fontName = @"CiutadellaRun-SemiBoldItalic";
            break;
            
        case GQFontTypeSemiBold:
            fontName = @"CiutadellaRun-SemiBold";
            break;
            
        default:
            fontName = @"HelveticaNeue";
            break;
    }
    
    return [UIFont fontWithName:fontName size:size];
}

@end
