//
//  GQLabel.m
//  GalileoQuiz
//
//  Created by Gihad Chbib on 24.03.14.
//  Copyright (c) 2014 ProSieben Games. All rights reserved.
//

#import "GQLabel.h"

@implementation GQLabel

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.font = [GQUtils fontForType:(GQFontType)self.tag size:self.font.pointSize];
}

@end
