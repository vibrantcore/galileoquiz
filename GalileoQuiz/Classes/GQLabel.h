//
//  GQLabel.h
//  GalileoQuiz
//
//  Created by Gihad Chbib on 24.03.14.
//  Copyright (c) 2014 ProSieben Games. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GQLabel : UILabel

@end
