//
//  main.m
//  GalileoQuiz
//
//  Created by Gihad Chbib on 01.03.14.
//  Copyright (c) 2014 ProSieben Games. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GQAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([GQAppDelegate class]));
    }
}
